<?php
/**
 * PHP FIG
 *
 * @category     PSR-13
 * @package      Link
 * @license      MIT. See also the LICENCE
 */
namespace Tiat\Psr\Link;

//

/**
 * A link provider object.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface LinkProviderInterface extends Psr\Link\LinkProviderInterface {
	
	/**
	 * Returns an iterable of LinkInterface objects.
	 * The iterable may be an array or any PHP \Traversable object. If no links
	 * are available, an empty array or \Traversable MUST be returned.
	 *
	 * @return iterable
	 * @since   3.0.0 First time introduced.
	 */
	public function getLinks() : iterable;
	
	/**
	 * Returns an iterable of LinkInterface objects that have a specific relationship.
	 * The iterable may be an array or any PHP \Traversable object. If no links
	 * with that relationship are available, an empty array or \Traversable MUST be returned.
	 *
	 * @return iterable
	 * @since   3.0.0 First time introduced.
	 * /
	 */
	public function getLinksByRel($rel) : iterable;
}
